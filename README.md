# README #

This is an ASP.NET mvc sample web application using. It consists of two user forms which send data to a entity framework code-first database. The application also includes and API which pulls data from the database and presents it to the end user. Right now the connection strings are configured to work while the project hosted in Azure cloud - this means that the API will not work in its current state when the application is run locally.

The requirements for the project are described in [this document](http://bit.ly/29Ay7xg). (Note: the PDF is in Estonian)
This version of the project does not include unit tests.