﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Link
{
    public class Website
    {
        public int WebsiteId { get; set; }
        [Required]
        public string Url { get; set; }
        public string Description { get; set; }
        [Required]
        public string Category { get; set; }
        public string Owner { get; set; }
        [Range(1, 10)]
        public int Points { get; set; }
        [Required]
        public string Title { get; set; }
        public int PersonId { get; set; }

        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
    }
}