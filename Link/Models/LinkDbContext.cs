﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Link.Models
{
    public class LinkDbContext : DbContext
    {
        public LinkDbContext() : base("name=LinkDbContext")
        {
        }

        public System.Data.Entity.DbSet<Link.Person> People { get; set; }
        public System.Data.Entity.DbSet<Link.Website> Websites { get; set; }
    }
}
