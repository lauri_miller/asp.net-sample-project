﻿#region Auto generated information. Please do not modify

// Link Link PeopleRepository.cs
// OTTPC 
// 2016-09-02 22:17
// 2016-09-02 22:03

#endregion

using System;
using System.Linq;
using System.Threading.Tasks;
using Link.Models;

namespace Link.DAL
{
    public class PeopleRepository : IPeopleRepository
    {
        private readonly LinkDbContext db = new LinkDbContext();

        public async Task<Person> CreatePerson(Person person)
        {
            var duplicatePersonsInDb =
                db.People.Count(
                    x =>
                        x.DateOfBirth == person.DateOfBirth && x.FirstName == person.FirstName
                        && x.LastName == person.LastName);

            if(duplicatePersonsInDb == 0)
            {
                person.DateCreated = DateTime.Now;
                person.DateModified = DateTime.Now;
                db.People.Add(person);
                await db.SaveChangesAsync();
                return person;
            }
            return null;
        }
    }
}
