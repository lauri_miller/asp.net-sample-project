using System.Threading.Tasks;

namespace Link.DAL
{
    public interface IPeopleRepository {
        Task<Person> CreatePerson(Person person);
    }
}