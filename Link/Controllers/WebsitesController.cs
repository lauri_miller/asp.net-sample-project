﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Link;
using Link.Models;
using Link.Services;

namespace Link.Controllers
{
    public class WebsitesController : Controller
    {
        private LinkDbContext db = new LinkDbContext();
        ZeldaService zeldaService = new ZeldaService();
        Website[] listOfWebsites = new Website[10];

        // GET: Websites
        public async Task<ActionResult> Index()
        {
            return View(await zeldaService.GetWebsitesAsync());
        }

        public ActionResult Create(int id)
        {
            return View(listOfWebsites);
        }

        [HttpPost]
        public  ActionResult Create(Website[] data, int id)
        {
            List<Website> dataToDb = new List<Website>();
            //Check if there are any websites with the same personId
            int count = db.Websites.Where(x => x.PersonId == id).Count();
            foreach (var item in data)
            {
                bool correct = true;
                if(count == 0 && item.Url != null && item.Title != null && item.Points >= 1 && item.Points <= 10 && item.Category != null && correct)
                {
                    dataToDb.Add(item);
                } 
                }

            if(dataToDb.Count() >= 5)
            {
                foreach (var item in dataToDb)
                {
                    item.DateCreated = DateTime.Now;
                    item.DateModified = DateTime.Now;
                    if (!String.IsNullOrEmpty(item.Url))
                        item.PersonId = id;
                        db.Websites.Add(item);
                }

                db.SaveChanges();
                return RedirectToAction("Index", "Websites");
            }

            Response.Write("<script>alert('The websites were not inserted to the database, because there were fewer than five correct inputs.')</script>");
            return View();

        }
    }
}
