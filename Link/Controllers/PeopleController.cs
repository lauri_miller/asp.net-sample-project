﻿#region Auto generated information. Please do not modify

// Link Link PeopleController.cs
// OTTPC 
// 2016-09-02 22:23
// 2016-09-02 21:58

#endregion

using System.Data.Entity;
using System.Threading.Tasks;
using System.Web.Mvc;
using Link.DAL;
using Link.Models;

namespace Link.Controllers
{
    public class PeopleController : Controller
    {
        public PeopleController(IPeopleRepository peopleRepository)
        {
            this.peopleRepository = peopleRepository;
        }

        private IPeopleRepository peopleRepository;
        private readonly LinkDbContext db = new LinkDbContext();
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(
            [Bind(Include = "PersonId,FirstName,LastName,DateOfBirth,DateCreated,DateModified")] Person person)
        {
            if(ModelState.IsValid)
            {
                var createdPerson = peopleRepository.CreatePerson(person);
                if(createdPerson != null)
                {
                    return RedirectToAction("Create", "Websites", new {id = person.PersonId});
                }
                Response.Write(
                    "<script>alert('This person already exists in the database.')</script>");

            }

            return View(person);
        }

        protected override void Dispose(bool disposing)
        {
            if(disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
