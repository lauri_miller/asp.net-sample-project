﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Link.Models;

namespace Link.Controllers
{
    public class ZeldaController : ApiController
    {
        private LinkDbContext db = new LinkDbContext();
        List<Website> TopScores = new List<Website>();
        public IEnumerable<Website> GetTopWebsites()
        {
            foreach (Website website in db.Websites.ToList())
            {
                if (TopScores.Any(x => x.Url == website.Url )){
                    TopScores.FirstOrDefault(x => x.Url == website.Url).Points += website.Points;
                }
                else
                {
                    TopScores.Add(website);
                }
            }
            return TopScores.OrderByDescending(o => o.Points).Take(10);
        }
    }
}
