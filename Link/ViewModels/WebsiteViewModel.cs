﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Link.ViewModels
{
    public class WebsiteViewModel
    {
        public List<Website> WebsiteList { get; set; }
    }
}