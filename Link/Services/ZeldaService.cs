﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace Link.Services
{
    public class ZeldaService
    {
        private string websitesUri = "http://localhost:51468/api/zelda";
        //GET top websites
        public async Task<List<Website>> GetWebsitesAsync()
        {
            using (HttpClient httpClient = new HttpClient())
            {
                return JsonConvert.DeserializeObject<List<Website>>(
                    await httpClient.GetStringAsync(websitesUri)
                );
            }
        }
    }
}